FROM ubuntu:14.04

ARG BUILD_DATE
ARG BUILD_VERSION
ARG DEBUG=1

LABEL maintainer="Michal Novacek <michal.novacek@gmail.com>"
LABEL description="\
Dockerfile used to build this image is here: \
https://gitlab.cee.redhat.com/misacek/docker-images/blob/acroread/docker/Dockerfile \
"
LABEL build_version="$BUILD_VERSION"
LABEL build_date="$BUILD_DATE"

ENV DEBIAN_FRONTEND noninteractive
ENV ACROREAD_PACKAGE ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
ENV uid 1000
ENV gid 1000

RUN useradd -m acroread && \
    dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        wget \
        cups-bsd \
        libcups2 \
        libgtk2.0-0:i386 \
        libnss3-1d:i386 \
        libnspr4-0d:i386 \
        lib32nss-mdns \
        libxml2:i386 \
        libxslt1.1:i386 \
        libstdc++6:i386 && \
    rm -rf /var/lib/apt/lists/* && \
    wget -q $ACROREAD_PACKAGE -O /tmp/acroread.deb && \
    dpkg -i  /tmp/acroread.deb && \
    rm /tmp/acroread.deb

COPY start-acroread.sh /
RUN chmod 755 /start-acroread.sh

ENTRYPOINT ["/start-acroread.sh"]
