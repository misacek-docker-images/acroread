
.DEFAULT_GOAL := all

include $(shell pwd)/.env
#include $(shell pwd)/Makefile.variables
MYSELF := TOP/Makefile
MY_DIR := $(shell pwd)
LOGFILE ?= $(MY_DIR)/build.log

ifdef PROJECT_DOCKER_HOST
DOCKER_HOST := ${PROJECT_DOCKER_HOST}
else
DOCKER_HOST := le-registry.le-domain.le-com
endif

ifdef PROJECT_DOCKER_COMPOSE_SERVICES
DOCKER_SERVICES := $(PROJECT_DOCKER_COMPOSE_SERVICES)
else
DOCKER_SERVICES := defult-to-be-changed
endif

ifdef PROJECT_IMAGE_NAMES
IMAGE_NAMES := $(PROJECT_IMAGE_NAMES:%=$(DOCKER_HOST)/%)
else
IMAGE_NAMES := $(DOCKER_HOST)/le-user/le-project/$(SERVICE)
endif

include $(shell pwd)/common/Makefile.checks
include $(shell pwd)/common/Makefile.docker-image-build

help: help-checks help-docker-image-build

show-vars: show_vars
show_vars: show-checks find_image_version
	@echo DOCKER_HOST: $(DOCKER_HOST)
	@echo IMAGE_NAMES: $(IMAGE_NAMES)
	@echo IMAGE_VERSION: $(IMAGE_VERSION)
	@echo DOCKER_SERVICES: $(DOCKER_SERVICES)
	@echo LOGFILE: $(LOGFILE)
	@echo

check: check-docker-compose check-docker check-sudo
	@echo $(shell date) $(MYSELF): success: $@ target

all: image
	@echo $(shell date) $(MYSELF): success: $@ target

