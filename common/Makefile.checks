

DOCKER = /usr/bin/docker
DOCKER_COMPOSE = /usr/local/bin/docker-compose
GIT = /usr/bin/git
SUDO = /usr/bin/sudo
ID = /usr/bin/id

HAVE_DOCKER := $(shell $(DOCKER) --version 2>/dev/null)
HAVE_DOCKER_COMPOSE := $(shell $(DOCKER_COMPOSE) --version 2>/dev/null)
HAVE_GIT := $(shell $(GIT) --version 2>/dev/null)
HAVE_SUDO := $(shell $(SUDO) -V 2>/dev/null | head -n 1)
HAVE_ID := $(shell $(ID) --version)

ifdef HAVE_ID
MY_ID := $(shell $(ID) --user 2>/dev/null)
ifeq (MY_ID,'0')
IS_ROOT = 'yes'
SUDO :=
else
IS_ROOT = 'no'
endif
endif

help-checks:
	@echo
	@echo "$@ targets:"
	@echo "check-git, check-sudo, check-docker, check-docker-compose, show-checks"

check-git:
ifndef HAVE_GIT
	$(error You need to have git installed.)
	@/bin/false
else
	@cho $(HAVE_GIT) is installed.
	@echo $(shell date) $(MYSELF): success: $@ target
endif

check-sudo:
ifndef HAVE_SUDO
	$(error You need to have sudo installed.)
else
	@echo $(HAVE_SUDO) is installed.
	@echo $(shell date) $(MYSELF): success: $@ target
endif

check-docker:
ifndef HAVE_DOCKER
	$(error You need to have docker installed.)
else
	@echo $(HAVE_DOCKER) is installed.
	@echo $(shell date) $(MYSELF): success: $@ target
endif

check-docker-compose:
ifndef HAVE_DOCKER_COMPOSE
	$(error You need to have docker-compose installed.)
else
	@echo $(HAVE_DOCKER_COMPOSE) is installed.
	@echo $(shell date) $(MYSELF): success: $@ target
endif

show-checks: show_checks
show_checks:
	@echo HAVE_DOCKER: $(HAVE_DOCKER)
	@echo HAVE_DOCKER_COMPOSE: $(HAVE_DOCKER_COMPOSE)
	@echo HAVE_GIT: $(HAVE_GIT)
	@echo HAVE_SUDO: $(HAVE_SUDO)
	@echo
	@echo IS_ROOT: $(IS_ROOT)
	@echo SUDO: $(SUDO)
	@echo

