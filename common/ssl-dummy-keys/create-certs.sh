#!/bin/bash
openssl req -new -x509 -nodes -days 3650 -newkey rsa:4096 -keyout key.pem -out cert.pem -subj "/CN=localhost"
