#!/bin/bash
export CONTAINER_SCREEN_WIDTH=${CONTAINER_SCREEN_WIDTH:-'1024'}
export CONTAINER_SCREEN_HEIGHT=${CONTAINER_SCREEN_HEIGHT:-'768'}
export CONTAINER_SCREEN_DEPTH=${CONTAINER_SCREEN_DEPTH:-'24'}
export CONTAINER_DISPLAY=${CONTAINER_DISPLAY:-':0'}
export CONTAINER_VNC_PASSWORD=${CONTAINER_VNC_PASSWORD:-'password'}
export CONTAINER_USER=${CONTAINER_USER:-'jalbum'}

export GEOMETRY="$CONTAINER_SCREEN_WIDTH""x""$CONTAINER_SCREEN_HEIGHT""x""$CONTAINER_SCREEN_DEPTH"

export CONTAINER_SSL_CERTIFICATE=${PROJECT_SSL_CERTIFICATE:-'/certs/certificate.pem'}
export CONTAINER_SSL_PRIVATE_KEY=${PROJECT_SSL_PRIVATE_KEY:-'/certs/private-key.pem'}

export DISPLAY=${DISPLAY:-':1'}
[ -n "$DEBUG" ] && set -x

function shutdown {
    kill -s SIGTERM $NODE_PID
    wait $NODE_PID
}

Xvfb $CONTAINER_DISPLAY -screen 0 $GEOMETRY &
NODE_PID=$!

#trap shutdown SIGTERM SIGINT
for i in $(seq 1 10)
do
    xdpyinfo -display $CONTAINER_DISPLAY >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "vnc server started"
        break
    fi
    echo Waiting xvfb...
    sleep 0.5
done

fluxbox -display $CONTAINER_DISPLAY &

#x11vnc -ncache -forever -bg -nopw -xkb -usepw -shared -rfbport 5900 -display $DISPLAY

# create menu entry for jalbum and xterm
mkdir -p ~/.fluxbox
cat << EOT > ~/.fluxbox/menu
[begin]
[exec] (term) {xvt -e /bin/bash}
[exec] (jalbum 13.10) {java -jar /jAlbum/JAlbum.jar}
[reconfig] (Reload config)
[exit] (Exit)
[end]
EOT
cat ~/.fluxbox/menu

[ -f "${CONTAINER_SSL_CERTIFICATE}" ]
[ -f "${CONTAINER_SSL_PRIVATE_KEY}" ]
cat ${CONTAINER_SSL_CERTIFICATE} ${CONTAINER_SSL_PRIVATE_KEY}  > /certs/server.pem
ln -fs ${CONTAINER_SSL_CERTIFICATE} /certs/server.crt

x11vnc -storepasswd ${CONTAINER_VNC_PASSWORD} /home/${CONTAINER_USER}/.vnc/passwd
x11vnc -display ${CONTAINER_DISPLAY} -bg -nopw -xkb -usepw -shared -repeat -loop -forever -ssldir /certs -ssl SAVE &

wait $NODE_PID
