#!/bin/bash -x

groupmod -g $gid acroread
usermod -u $uid -g $gid acroread

if [ -d /home/acroread/.adobe ];
then
    chown -R acroread:acroread /home/acroread/.adobe
fi

echo "export CUPS_SERVER=$CUPS_SERVER" >> ~acroread/.bashrc

exec su -ls "/bin/bash" -c "mkdir -p /home/acroread/.local/share; /usr/bin/acroread '$ARGS' '$FILE'" acroread
